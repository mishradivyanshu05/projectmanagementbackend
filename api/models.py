# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True)
    duration = models.IntegerField(null=True)
class Tasks(models.Model):
    project = models.ForeignKey(Project,on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    description = models.TextField(null=True)
    duration = models.IntegerField(null=True)