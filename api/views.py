import datetime
import json
import random
import requests
import os,sys

from django.db.models import Q
from django.shortcuts import redirect
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import authenticate

from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework import generics
from rest_framework.parsers import MultiPartParser, FormParser
from django.http import HttpResponse
from django.contrib.auth.models import User
from .models import Project,Tasks
from .serializers import ProjectSerializer,TaskSerializer

from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
#from .tokens import account_activation_token
from django.core.mail import EmailMessage
#from .utils import

class GetProjects(APIView):
    def get(self, request):
        project_obj = Project.objects.all()
        projects = ProjectSerializer(instance=project_obj,many=True)
        return Response({"projectlist":projects.data,"flag":True},status=status.HTTP_200_OK)

class Task(APIView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        taskobj = Tasks.objects.filter(project__id=int(pk))
        print(taskobj)
        serializer = TaskSerializer(instance=taskobj, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)
    def post(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        proobj = Project.objects.get(id=int(pk))
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            data=serializer.save(project=proobj)
            return Response({'messege':'task created'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
class TaskDetail(APIView):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        taskobj = Tasks.objects.get(id=int(pk))
        serializer = TaskSerializer(instance=taskobj)
        return Response(serializer.data,status=status.HTTP_200_OK)
    def put(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        taskobj = Tasks.objects.get(id=int(pk))
        serializer = TaskSerializer(instance=taskobj,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    def delete(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        taskobj = Tasks.objects.get(id=int(pk))
        taskobj.delete()
        return Response({"messege":"Operation Success","flag":True},status=status.HTTP_201_CREATED)

class CreateProject(APIView):
    def get(self, request, *args, **kwargs):
        pk=self.kwargs.get("pk")
        proj_obj = Project.objects.get(id=int(pk))
        serializer = ProjectSerializer(instance=proj_obj)
        return Response(serializer.data,status=status.HTTP_200_OK)
    def post(self, request):
        # name = request.data.get("name")
        # description = request.data.get("description")
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"message":"Project Created","flag":True},status=status.HTTP_201_CREATED)
        else:
            return Response({"message":serializer.errors,"flag":False},status=status.HTTP_400_BAD_REQUEST)
    def put(self, request, *args, **kwargs):
        pk=self.kwargs.get("pk")
        proj_obj = Project.objects.get(id=int(pk))
        serializer = ProjectSerializer(instance=proj_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk")
        proj_obj = Project.objects.get(id=int(pk))
        proj_obj.delete()
        return Response({"messege":"Operation Success","flag":True},status=status.HTTP_201_CREATED)
class ProjectDetail(APIView):
    def get(self, request, *args, **kwargs):
        pk=self.kwargs.get("pk")
        proj_obj = Project.objects.get(id=int(pk))
        serializer = ProjectSerializer(instance=proj_obj)
        return Response(serializer.data,status=status.HTTP_200_OK)
# class ProjectOperation(generics.RetrieveUpdateDestroyAPIView):
#     serializer_class = ProjectSerializer
#
#     def get_serializer(self, *args, **kwargs):
#         kwargs['partial'] = True
#         return super(ProductOperation, self).get_serializer(*args, **kwargs)
#
#     def get_queryset(self):
#         return Product.objects.filter(shop=ShopDetail.objects.get(user=self.request.user),pk=int(self.kwargs.get("pk")))
#
#     def perform_update(self, serializer):
#         pro_obj=serializer.save()
#         pur_rate=convert_to_float(pro_obj.pur_rate)
#         quantity=convert_to_float(pro_obj.quantity)
#         updated=(pro_obj.id,-1,quantity,pur_rate,pur_rate*quantity,"O",get_date(pro_obj.added),"O_"+str(pro_obj.id))
#         delete_stock(self.request.user.username,[str("O_")+str(pro_obj.id)])
#         execute_query(self.request.user.username,updated)
