from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'api/CreateProject/$',views.CreateProject.as_view(),name='createproject'),
    url(r'api/getProjects/$',views.GetProjects.as_view(),name='getproject'),
    url(r'api/CreateProject/(?P<pk>[0-9]+)/$', views.CreateProject.as_view(), name='edit-project'),
    url(r'api/ProjectDetail/(?P<pk>[0-9]+)/$', views.ProjectDetail.as_view(), name='project-detail'),
    url(r'api/Task/(?P<pk>[0-9]+)/$', views.Task.as_view(), name='tasks'),
    url(r'api/TaskDetail/(?P<pk>[0-9]+)/$', views.TaskDetail.as_view(), name='task-detail'),
]
