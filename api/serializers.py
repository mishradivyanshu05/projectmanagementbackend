from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response

from .models import Project,Tasks

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields='__all__'
class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields=('id','name','description','duration')
